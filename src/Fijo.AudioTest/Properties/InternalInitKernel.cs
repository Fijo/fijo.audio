using Fijo.Audio.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;

namespace Fijo.AudioTest.Properties {
	public class InternalInitKernel : ExtendedInitKernel {
		public override void PreInit() {
			LoadModules(new AudioInjectionModule());
		}
	}
}