﻿using Fijo.Audio.Feedback;
using Fijo.AudioTest.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;

namespace Fijo.AudioTest.Feedback {
	[TestFixture]
	public class BeepServiceTest {
		private IBeepService _beepService;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_beepService = Kernel.Resolve<IBeepService>();
		}

		[Test, Ignore]
		public void Beep() {
			_beepService.Beep();
		}
	}
}
