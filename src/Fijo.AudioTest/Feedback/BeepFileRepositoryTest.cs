using System.Diagnostics;
using System.IO;
using Fijo.Audio.Feedback;
using NUnit.Framework;

namespace Fijo.AudioTest.Feedback {
	[TestFixture]
	public class BeepFileRepositoryTest {
		[Test, Ignore]
		public void GenerateByteArraySourceFromFile() {
			var content = string.Join(", ", File.ReadAllBytes(@"C:\Projekte\Neuer Ordner (2)\res\sound\beep\BiosBeep.mp3"));
			Debug.WriteLine(string.Format(@"new byte[]{{ {0} }}", content));
		}
		[Test]
		public void Get() {
			var beepRepository = new BeepFileRepository();
			var file = beepRepository.Get();
			Assert.True(File.Exists(file));
		}
	}
}