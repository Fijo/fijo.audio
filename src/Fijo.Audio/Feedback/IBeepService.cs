namespace Fijo.Audio.Feedback {
	public interface IBeepService {
		void Beep();
	}
}