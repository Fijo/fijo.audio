//BS:enter shikari- enter shikari

using System.Diagnostics;
using System.Threading;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;

namespace Fijo.Audio.Feedback {
	public class BeepService : IBeepService {
		private readonly string _beepFilePath = Kernel.Resolve<IBeepFileRepository>().Get();
		private readonly string _vlcBinaryPath = Kernel.Resolve<IConfigurationService>().Get<string>("Fijo.VCS.Git.VlcBinaryPath");

		public void Beep() {
			#if UseSpeakerBeep
			Console.Beep();
			#else
			new Thread(InternalBeep).Start();
			#endif
		}

		private void InternalBeep() {
			var procStartInfo = new ProcessStartInfo(_vlcBinaryPath, _beepFilePath) {RedirectStandardOutput = false, UseShellExecute = false, CreateNoWindow = true};
			using(var proc = new Process {StartInfo = procStartInfo}) {
				proc.Start();
				Thread.Sleep(800);
				proc.Kill();
			}
			Thread.CurrentThread.Abort();
		}
	}
}