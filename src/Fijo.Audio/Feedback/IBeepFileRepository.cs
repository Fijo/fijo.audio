namespace Fijo.Audio.Feedback {
	public interface IBeepFileRepository {
		string Get();
	}
}