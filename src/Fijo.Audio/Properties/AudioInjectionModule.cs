﻿using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace Fijo.Audio.Properties {
	public class AudioInjectionModule : ExtendedNinjectModule {
    	public override void AddModule(IKernel kernel) {
    		kernel.Load(new AudioFeedbackInjectionModule());
    		kernel.Load(new AudioPlayerInjectionModule());
    	}
    }
}
