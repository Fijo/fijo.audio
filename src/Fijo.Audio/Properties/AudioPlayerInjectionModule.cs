using Fijo.Audio.Player;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace Fijo.Audio.Properties {
	public class AudioPlayerInjectionModule : ExtendedNinjectModule {
		public override void OnLoad(IKernel kernel)
		{
			kernel.Bind<IMusicPlayerService>().To<WinMediaPlayerService>().InSingletonScope();
			kernel.Bind<IMusicPlayerFacade>().To<MusicPlayerFacade>().InSingletonScope();
		}
	}
}