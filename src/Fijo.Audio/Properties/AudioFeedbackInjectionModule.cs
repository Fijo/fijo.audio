using Fijo.Audio.Feedback;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Properties;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace Fijo.Audio.Properties {
	public class AudioFeedbackInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new LightContribInjectionModule());
		}

		public override void OnLoad(IKernel kernel)
		{
			kernel.Bind<IBeepFileRepository>().To<BeepFileRepository>().InSingletonScope();
			kernel.Bind<IBeepService>().To<BeepService>().InSingletonScope();
		}
	}
}