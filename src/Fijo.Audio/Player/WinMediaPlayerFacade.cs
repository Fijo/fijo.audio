using FijoCore.Infrastructure.DependencyInjection.InitKernel;

namespace Fijo.Audio.Player {
	public class MusicPlayerFacade : IMusicPlayerFacade {
		private readonly IMusicPlayerService _musicPlayerService = Kernel.Resolve<IMusicPlayerService>();

		#region Implementation of IMusicPlayerService
		public MusicPlayer Create() {
			return _musicPlayerService.Create();
		}

		public void SetUrl(MusicPlayer mp, string file) {
			_musicPlayerService.SetUrl(mp, file);
		}

		public void Play(MusicPlayer mp) {
			_musicPlayerService.Play(mp);
		}

		public void Play(MusicPlayer mp, string file) {
			if(IsPlaying(mp)) Stop(mp);
			SetUrl(mp, file);
			Play(mp);
		}

		public double GetPosition(MusicPlayer mp) {
			return _musicPlayerService.GetPosition(mp);
		}

		public void SetPosition(MusicPlayer mp, double position) {
			_musicPlayerService.SetPosition(mp, position);
		}

		public double GetDuration(MusicPlayer mp) {
			return _musicPlayerService.GetDuration(mp);
		}

		public bool IsPlaying(MusicPlayer mp) {
			return _musicPlayerService.IsPlaying(mp);
		}

		public void Stop(MusicPlayer mp) {
			_musicPlayerService.Stop(mp);
		}

		public void Pause(MusicPlayer mp) {
			_musicPlayerService.Pause(mp);
		}

		public void Close(MusicPlayer mp) {
			_musicPlayerService.Close(mp);
		}
		#endregion
	}
}