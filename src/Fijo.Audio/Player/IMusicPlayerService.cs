//BS:Enter Shikari - Ok, Time For Plan B

namespace Fijo.Audio.Player {
	public interface IMusicPlayerService {
		MusicPlayer Create();
		void SetUrl(MusicPlayer mp, string file);
		void Play(MusicPlayer mp);
		double GetPosition(MusicPlayer mp);
		void SetPosition(MusicPlayer mp, double position);
		double GetDuration(MusicPlayer mp);
		bool IsPlaying(MusicPlayer mp);
		void Stop(MusicPlayer mp);
		void Pause(MusicPlayer mp);
		void Close(MusicPlayer mp);
	}
}