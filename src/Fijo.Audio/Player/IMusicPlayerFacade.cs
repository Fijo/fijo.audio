namespace Fijo.Audio.Player {
	public interface IMusicPlayerFacade {
		MusicPlayer Create();
		void SetUrl(MusicPlayer mp, string file);
		void Play(MusicPlayer mp);
		void Play(MusicPlayer mp, string file);
		double GetPosition(MusicPlayer mp);
		void SetPosition(MusicPlayer mp, double position);
		double GetDuration(MusicPlayer mp);
		bool IsPlaying(MusicPlayer mp);
		void Stop(MusicPlayer mp);
		void Pause(MusicPlayer mp);
		void Close(MusicPlayer mp);
	}
}