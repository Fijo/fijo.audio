//BS:Enter Shikari - Ok, Time For Plan B

using System;
#if WindowsMediaPlayer
using System.Diagnostics;
using System.IO;
using WMPLib;
#endif

namespace Fijo.Audio.Player {
	public class WinMediaPlayerService : IMusicPlayerService {

		public WinMediaPlayerService() {
			#if !WindowsMediaPlayer
			throw GetNotSupported();
			#endif
		}

		private Exception GetNotSupported() {
			#if !WindowsMediaPlayer
			return new NotSupportedException("compiled without /define WindowsMediaPlayer - WindowsMediaPlayer is not supported");
			#else
			return new NotSupportedException();
			#endif
		}

		public MusicPlayer Create() {
			#if WindowsMediaPlayer
			return new MusicPlayer {Instance = InteranalCreate()};
			#else
			throw GetNotSupported();
			#endif
		}
		
		#if WindowsMediaPlayer
		private WindowsMediaPlayer InteranalCreate() {
			return new WindowsMediaPlayer();
		}
		#endif
		
		public void SetUrl(MusicPlayer mp, string file) {
			#if WindowsMediaPlayer
			Debug.Assert(File.Exists(file), "File.Exists(file)");
			GetInstance(mp).URL = file;
			#else
			throw GetNotSupported();
			#endif
		}

		public void Play(MusicPlayer mp) {
			#if WindowsMediaPlayer
			GetInstance(mp).controls.play();
			#else
			throw GetNotSupported();
			#endif
		}
		
		public double GetPosition(MusicPlayer mp) {
			#if WindowsMediaPlayer
			return GetInstance(mp).controls.currentPosition;
			#else
			throw GetNotSupported();
			#endif
		}
		
		public void SetPosition(MusicPlayer mp, double position) {
			#if WindowsMediaPlayer
			GetInstance(mp).controls.currentPosition = position;
			#else
			throw GetNotSupported();
			#endif
		}
		
		public double GetDuration(MusicPlayer mp) {
			#if WindowsMediaPlayer
			return GetInstance(mp).currentMedia.duration;
			#else
			throw GetNotSupported();
			#endif
		}
		
		public bool IsPlaying(MusicPlayer mp) {
			#if WindowsMediaPlayer
			return GetInstance(mp).playState == WMPPlayState.wmppsPlaying;
			#else
			throw GetNotSupported();
			#endif
		}

		public void Stop(MusicPlayer mp) {
			#if WindowsMediaPlayer
			GetInstance(mp).controls.stop();
			#else
			throw GetNotSupported();
			#endif
		}

		public void Pause(MusicPlayer mp) {
			#if WindowsMediaPlayer
			GetInstance(mp).controls.pause();
			#else
			throw GetNotSupported();
			#endif
		}

		public void Close(MusicPlayer mp) {
			#if WindowsMediaPlayer
			GetInstance(mp).close();
			#else
			throw GetNotSupported();
			#endif
		}
		
		#if WindowsMediaPlayer
		private WindowsMediaPlayer GetInstance(MusicPlayer mp) {
			Debug.Assert(mp.Instance is WindowsMediaPlayer, "mp.Instance is WindowsMediaPlayer");
			return (WindowsMediaPlayer) mp.Instance;
		}
		#endif
	}
}